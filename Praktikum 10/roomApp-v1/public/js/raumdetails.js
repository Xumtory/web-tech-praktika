class Room {
    constructor(number, label, building, capacity, equipment) {
        this.number = number;
        this.label = label;
        this.building = building;
        this.capacity = capacity;
        this.equipment = equipment;
        this.bookings = [];
    }

    addBooking(booking) {
        this.bookings.push(booking);
        this.bookings.sort((x, y) => { y.start - x.start });
    }
}

class Buchung {
    constructor(label, start, end, bookedBy, countVisit, desc) {
        this.label = label;
        this.start = start;
        this.end = end;
        this.bookedBy = bookedBy;
        this.countVisit = countVisit;
        this.desc = desc;
    }
}

let room = new Room(42, "A", "FB4", 64, "abc", "cde");
room.addBooking(new Buchung("Lol - A", new Date(2020, 11, 15, 10), new Date(2020, 8, 15, 11, 30), "Herr A", 50, "Vorlesung: SWT1"));
room.addBooking(new Buchung("Lol - B", new Date(2020, 11, 15, 13), new Date(2020, 11, 15, 14, 30), "Herr B", 50, "Übung: SWT1"));
room.addBooking(new Buchung("Lol - C", new Date(2020, 11, 16, 8, 30), new Date(2020, 11, 16, 10), "Herr C", 50, "Vorlesung: Web-Tech"));

let list = document.getElementById("detailList");

//Detail area
addDetail("Raumnummer", room.number);
addDetail("Bezeichnung", room.label);
addDetail("Gebäude", room.building);
addDetail("Kapazität", room.capacity);
addDetail("Ausstatung", room.equipment);

function addDetail(key, value) {
    let a = document.createElement("li");
    a.innerHTML = `<strong>${key}:</strong> ${value}`;
    list.append(a);
}

//Table area
let table = document.getElementsByTagName("tbody")[0];

for (let i of room.bookings) {
    addItem(i);
}

function addItem(row) {
    let tr = document.createElement("tr");
    let date = document.createElement("td");
    let start = document.createElement("td");
    let end = document.createElement("td");
    let desc = document.createElement("td");
    date.textContent = row.start.toLocaleDateString();
    start.textContent = (row.start.getHours() <= 9 ? "0" + row.start.getHours() : row.start.getHours()) + ":" + (row.start.getMinutes() <= 9 ? "0" + row.start.getMinutes() : row.start.getMinutes());
    end.textContent = (row.end.getHours() <= 9 ? "0" + row.end.getHours() : row.end.getHours()) + ":" + (row.end.getMinutes() <= 9 ? "0" + row.end.getMinutes() : row.end.getMinutes());
    desc.textContent = row.desc;
    tr.append(date);
    tr.append(start);
    tr.append(end);
    tr.append(desc);
    table.append(tr);
}
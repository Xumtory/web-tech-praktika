let kachel = document.createElement("a");
kachel.className = "button";
document.querySelector("main a:last-child").after(kachel);
kachel.textContent = "+";
kachel.addEventListener("click", function (event) {
    event.preventDefault();
    let name = prompt("Namen eingeben:");
    let url = prompt("Link eingeben:");
    if (name === null || name.length === 0 || url === null) {
        return;
    }
    let kachel2 = document.createElement("a");
    kachel2.className = "button";
    kachel2.href = url.includes("http") ? url : `http://${url}`;
    kachel2.textContent = name;
    kachel.before(kachel2);
})

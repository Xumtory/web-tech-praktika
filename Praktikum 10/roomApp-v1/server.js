const http = require("http");
const fs = require("fs");
const path = require("path");

//Starte server.
const server = http.createServer(function (request, response) {
  let url = request.url;
  if (url === "/") {
    url = "/dashboard.html";
  } else if (url === "/raumliste.html") {
    response.writeHead(200);
    response.end(createRaumHtml());
    return;
  } else if (url.endsWith("/")) {
    url = url.substring(0, url.length - 1);
    response.writeHead(302, { "Location": url });
    response.end();
    return;
  }
  let filename = `public${url}`;
  //Lese angeforderte Datei
  fs.readFile(filename, function (err, file) {
    if (err) {
      response.writeHead(302, { "Location": "/404.html" });
      response.end();
      return;
    }
    //Schreibe Datei in response
    response.writeHead(200);
    response.end(file);
  });
}).listen(8020);

class Room {
  constructor(number, label, building, capacity, equipment) {
    this.number = number;
    this.label = label;
    this.building = building;
    this.capacity = capacity;
    this.equipment = equipment;
    this.bookings = [];
  }

  addBooking(booking) {
    this.bookings.push(booking);
    this.bookings.sort((x, y) => { y.start - x.start });
  }
}

let rooms = [new Room("A.E.01", "Saal EF42"), new Room("A.1.01", "Vorlesung 1.OG"), new Room("A.3.01", "Übungsraum 3.OG")];

function createRaumListe() {
  let a = "";
  for (const i of rooms) {
    a = `${a}
    <li><a href="./raumdetails.html">${i.number}</a>${i.label}</li>`;
  }
  return a;
}

function createRaumHtml() {
  return `<!DOCTYPE html>
  <html lang="de">
  
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Raumliste</title>
    <link rel="stylesheet" type="text/css" href="./css/style.css" />
    <link rel="stylesheet" type="text/css" href="./css/flexbox.css" />
    <script src="./js/myScript.js"></script> 
  </head>
  
  <body>
    <header>
      <a href="./dashboard.html"> <img src="./img/logo.png" alt="Logo der Gruppe" width="200" /></a>
      <h1>Raumliste</h1>
    </header>
    <nav>
      <ul>
        <li>
          Raumliste
        </li>
        <li><a href="./raumdetails.html">Raumdetails</a>
        </li>
        <li>
          <a href="./buchungsdetails.html">Buchungsdetails</a>
        </li>
        <li>
          <a href="./seminar.html">Buchung anlegen</a>
        </li>
      </ul>
    </nav>
    <br>
    <div class="flex">
      <main>
        <form method="GET"
            action="https://labs.inf.fh-dortmund.de/roomReservationService/testRoomSearch.php">
        <fieldset>
          <legend>Raumsuche</legend>   
          <p>   
            <label for="raum" class="req">Raumnummer eingeben:</label>    
            <input id="raum" type="text" title="Raum genau wie im Beispiel angeben, z.B. A.E.01" name="roomno" required
              size="6" maxlength="6" placeholder="A.E.01" pattern="[A-Z]\.[E|1-3]{1}\.[0-9]{2}" />
          </p>    
          <p><input type="submit" /></p>     
        </fieldset> 
      </form>
      <div class="tblock">
        <h3>Räume:</h3>
        <ul>
          ${createRaumListe()}
        </ul>
      </div>
      </main>
      <aside >
        <h3>Aktuelle Meldungen</h3>
        <p><strong>31.10.2020:</strong> Beamer in Raum xxx ist defekt!</p>
        <p><strong>04.09.2022:</strong> Bitte keinen Permanentmarker verwenden!</p>
      </aside>
    </div>
    <footer>
      <hr />
      <div id="footer-text">
        © by ITAs Inc.
      </div>
    </footer>
  </body>
  
  </html>`;
}

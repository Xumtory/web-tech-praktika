function getViewportWidth() {
    return innerWidth || document.documentElement.clientWidth;
}

var str1 = "Die Viewport-Breite beträgt: ";
var str2 = `${str1}${getViewportWidth()} Pixel`
console.log(str2);

class Room {
    constructor(number, label, building, capacity, equipment) {
        this.number = number;
        this.label = label;
        this.building = building;
        this.capacity = capacity;
        this.equipment = equipment;
        this.bookings = [];
    }

    addBooking(booking) {
        this.bookings.push(booking);
        this.bookings.sort((x, y) => { y.start - x.start });
    }
}

class Buchung {
    constructor(label, start, end, bookedBy, countVisit, desc) {
        this.label = label;
        this.start = start;
        this.end = end;
        this.bookedBy = bookedBy;
        this.countVisit = countVisit;
        this.desc = desc;
    }
}

let roomA = new Room(42, "A", "FB4", 64, "abc", "cde");
let roomB = new Room(21, "B", "FB4", 32, "lost");

roomA.addBooking(new Buchung("Lol - A", new Date(2020, 8, 15), new Date(2020, 8, 16), "Herr A", 50, "Einfach so!"));
roomA.addBooking(new Buchung("Lol - B", new Date(2020, 11, 15), new Date(2020, 11, 16), "Herr B", 50, "Einfach so!"));
roomA.addBooking(new Buchung("Lol - C", new Date(2020, 7, 15), new Date(2020, 7, 16), "Herr C", 50, "Einfach so!"));

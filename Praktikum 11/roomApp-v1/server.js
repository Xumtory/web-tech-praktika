const http = require("http");
const fs = require("fs");
const path = require("path");
var data404;

//Lese die 404.html, so dass diese immer bereit ist
data404 = fs.readFileSync(path.join(__dirname,"public","404.html"));

//Starte server.
const server = http.createServer(function (request, response) {
  let url = request.url;
  if (url === "/") {
    url = "/dashboard.html";
  } else if (url === "/raumliste.html") {
    response.writeHead(200);
    response.end(createRaumHTML());
    return;
  } else if (url.endsWith("/")) {
    url = url.substring(0, url.length - 1);
    response.writeHead(302, { "Location": url });
    response.end();
    return;
  }
  let filename = `public${url}`;
  //Lese angeforderte Datei
  fs.readFile(filename, function (err, file) {
    if (err) {
      response.writeHead(404);
      response.end(data404);
      return;
    }
    //Schreibe Datei in response
    response.writeHead(200);
    response.end(file);
  });
}).listen(8020);


class Raum {
  constructor(num, bez, geb, kap, aus) {
    this.num = num;
    this.bez = bez;
    this.geb = geb;
    this.kap = kap;
    this.aus = aus;
  }
  buchungen = new Array();

  addBuchung(buch) {
    this.buchungen.push(buch);
    this.buchungen.sort(function (x, y) {
      return new Date(y.start) - new Date(x.start);
    });
  }
}

class Buchung {
  constructor(bez, start, end, von, anz, besch) {
    this.bez = bez;
    this.start = start;
    this.end = end;
    this.von = von;
    this.anz = anz;
    this.besch = besch;
  }
}

var rooms = [
  new Raum("A.E.01", "Vorlesungssaal", "EF42", 1000, ["3 Beamer", "Whiteboard", "Tafel"]),
  new Raum("A.E.02", "Vorlesungssaal", "EF42", 100, ["1 Beamer", "Whiteboard", "Tafel"]),
  new Raum("A.E.03", "Labor", "EF42", 30, ["1 Beamer", "Whiteboard", "Tafel", "30 Computer"]),
  new Raum("A.E.04", "Übungsraum", "EF42", 40, ["1 Beamer", "Whiteboard", "Tafel"])
];

function createRaumListe() {
  let result = "";
  for (let room of rooms) {
    result += `<li>Raumnummer: <strong>${room.num}</strong><ul><li>Bezeichnung: ${room.bez}</li></ul>`;
  }
  return result;
}

function createRaumHTML() {
  return `<!DOCTYPE html>
    <html lang="de">   
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <title>Raumliste</title>
      <link rel="stylesheet" type="text/css" href="css/style.css" />
      <link rel="stylesheet" type="text/css" href="css/flexbox.css" />
      <script src="js/myScript.js"></script> 
    </head>   
    <body>
      <header>
        <a href="dashboard.html"> <img src="img/logo.png" alt="Logo der Gruppe" width="200" /></a>
        <h1>Raumliste</h1>
      </header>
      <nav>
        <ul>
          <li>
            Raumliste
          </li>
          <li><a href="raumdetails.html">Raumdetails</a>
          </li>
          <li>
            <a href="buchungsdetails.html">Buchungsdetails</a>
          </li>
          <li>
            <a href="seminar.html">Buchung anlegen</a>
          </li>
        </ul>
      </nav>
      <br>
      <div class="flex">
        <main>
          <form method="GET"
              action="https://labs.inf.fh-dortmund.de/roomReservationService/testRoomSearch.php">
          <fieldset>
            <legend>Raumsuche</legend>   
            <p>   
              <label for="raum" class="req">Raumnummer eingeben:</label>    
              <input id="raum" type="text" title="Raum genau wie im Beispiel angeben, z.B. A.E.01" name="roomno" required
                size="6" maxlength="6" placeholder="A.E.01" pattern="[A-Z]\.[E|1-3]{1}\.[0-9]{2}" />
            </p>    
            <p><input type="submit" /></p>     
          </fieldset> 
        </form>
        <div class="tblock">
          <h3>Räume:</h3>
          <ul>
          ${createRaumListe()}
          </ul>
        </div>
        </main>
        <aside >
          <h3>Aktuelle Meldungen</h3>
          <p><strong>31.10.2020:</strong> Beamer in Raum xxx ist defekt!</p>
          <p><strong>04.09.2022:</strong> Bitte keinen Permanentmarker verwenden!</p>
        </aside>
      </div>
      <footer>
        <hr />
        <div id="footer-text">
          © by ITAs Inc.
        </div>
      </footer>
    </body>    
    </html>`;
}

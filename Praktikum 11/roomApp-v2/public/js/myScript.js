function getViewportWidth() {
    return window.innerWidth || document.documentElement.clientWidth;
}

var str1 = "Die Viewport-Breite beträgt: ";
var str2 = `${str1}${getViewportWidth()} Pixel`
console.log(str2);

class Raum {
    constructor(num, bez, geb, kap, aus) {
        this.num = num;
        this.bez = bez;
        this.geb = geb;
        this.kap = kap;
        this.aus = aus;
    }
    buchungen = new Array();

    addBuchung(buch) {
        this.buchungen.push(buch);
        this.buchungen.sort(function (x, y) {
            return new Date(y.start) - new Date(x.start);
        })
    }
}

class Buchung {
    constructor(bez, start, end, von, anz, besch) {
        this.bez = bez;
        this.start = start;
        this.end = end;
        this.von = von;
        this.anz = anz;
        this.besch = besch;
    }
}


raum1 = new Raum("A.E.01", "Vorlesungssaal", "EF42", 1000, ["3 Beamer", "Whiteboard", "Tafel"]);
raum2 = new Raum("A.E.02", "Vorlesungssaal(klein)", "EF42", 100, ["Beamer", "Whiteboard", "Tafel"]);
console.log(raum1);
console.log(raum2);
buchung1 = new Buchung("Übung", new Date("2020-01-04T12:00"), new Date("2020-01-04T14:00"), "Dr.Frank", 250, "Große Übung");
buchung2 = new Buchung("Vorlesung", new Date("2020-01-04T14:15"), new Date("2020-01-04T15:00"), "Dr.Frank", 250, "Vorlesung zu Thema xxx");
buchung3 = new Buchung("Praktikum", new Date("2020-01-04T15:15"), new Date("2020-01-04T16:00"), "Dr.Frank", 50, "Praktikum Gruppe a");

raum1.addBuchung(buchung1);
raum1.addBuchung(buchung2);
raum1.addBuchung(buchung3);

raum1.buchungen.forEach(function (elem) {
    console.log(elem);
});



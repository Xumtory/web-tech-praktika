class Raum {
    constructor(num, bez, geb, kap, aus) {
        this.num = num;
        this.bez = bez;
        this.geb = geb;
        this.kap = kap;
        this.aus = aus;
    }
    buchungen = new Array();

    addBuchung(buch) {
        this.buchungen.push(buch);
        this.buchungen.sort(function (x, y) {
            return new Date(y.start) - new Date(x.start);
        })
    }
}

class Buchung {
    constructor(bez, start, end, von, anz, besch) {
        this.bez = bez;
        this.start = start;
        this.end = end;
        this.von = von;
        this.anz = anz;
        this.besch = besch;
    }
}

let createTd = function (content) {
    let td = document.createElement("td");
    td.textContent = content;
    return td;
};


raum1 = new Raum("A.E.01", "Vorlesungssaal", "EF42", 1000, ["3 Beamer", "Whiteboard", "Tafel"]);
buchung1 = new Buchung("Übung", new Date("2020-01-04T12:00"), new Date("2020-01-04T14:00"), "Dr.Frank", 250, "Große Übung");
buchung2 = new Buchung("Vorlesung", new Date("2020-01-04T14:15"), new Date("2020-01-04T15:00"), "Dr.Frank", 250, "Vorlesung zu Thema xxx");
buchung3 = new Buchung("Praktikum", new Date("2020-01-04T15:15"), new Date("2020-01-04T16:00"), "Dr.Frank", 50, "Praktikum Gruppe a");
raum1.addBuchung(buchung1);
raum1.addBuchung(buchung2);
raum1.addBuchung(buchung3);

let liste = document.getElementsByClassName("liDetail");
lenListe = liste.length;

liste[0].append(" " + raum1.num);
liste[1].append(" " + raum1.bez);
liste[2].append(" " + raum1.geb);
liste[3].append(" " + raum1.kap);
liste[4].append(" " + raum1.aus);
console.log(liste);

len = raum1.buchungen.length;
let tableContent = new Array();

for(i = 0; i < len; i++){
    tableContent[i] = document.createElement("tr");
    tableContent[i].append(createTd(raum1.buchungen[i].start.toLocaleDateString()));
    tableContent[i].append(createTd(raum1.buchungen[i].start.toLocaleTimeString()));
    tableContent[i].append(createTd(raum1.buchungen[i].end.toLocaleTimeString()));
    tableContent[i].append(createTd(raum1.buchungen[i].besch));
}

for(i = 0; i < len; i++){
    document.querySelector("tbody").append(tableContent[i]);
}
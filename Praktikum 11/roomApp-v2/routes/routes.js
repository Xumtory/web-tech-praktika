const express = require("express");
const rooms = require("../models/rooms");

const router = express.Router();

router.get("/", (req, res) => res.render("dashboard"));

router.get("/raumliste", (req, res) => res.render("raumliste", { rooms }));

router.get("/raumdetail", (req, res) => res.render("raumdetails"));

router.get("/buchungsdetails", (req, res) => res.render("buchungsdetails"));

router.get("/neuebuchung", (req, res) => res.render("seminar"));

router.get("/404", (req, res) => res.status(404).render("404"));

router.use((req, res) => res.redirect("/404"));

module.exports = router;
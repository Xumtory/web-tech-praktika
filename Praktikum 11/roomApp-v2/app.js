const express = require("express");

const app = express();
const routes = require("./routes/routes");
const PORT = 8040;

app.set("view engine", "ejs");
app.set("views", "./views");

app.use(express.static("public"));
app.use(routes);

app.listen(PORT, () => console.log(`Server is listening on port ${PORT}...`));
class Raum {
    constructor(num, bez, geb, kap, aus) {
      this.num = num;
      this.bez = bez;
      this.geb = geb;
      this.kap = kap;
      this.aus = aus;
    }
    buchungen = new Array();
  
    addBuchung(buch) {
      this.buchungen.push(buch);
      this.buchungen.sort(function (x, y) {
        return new Date(y.start) - new Date(x.start);
      });
    }
  }
  
  class Buchung {
    constructor(bez, start, end, von, anz, besch) {
      this.bez = bez;
      this.start = start;
      this.end = end;
      this.von = von;
      this.anz = anz;
      this.besch = besch;
    }
  }
  
  var rooms = [
    new Raum("A.E.01", "Vorlesungssaal", "EF42", 1000, ["3 Beamer", "Whiteboard", "Tafel"]),
    new Raum("A.E.02", "Vorlesungssaal", "EF42", 100, ["1 Beamer", "Whiteboard", "Tafel"]),
    new Raum("A.E.03", "Labor", "EF42", 30, ["1 Beamer", "Whiteboard", "Tafel", "30 Computer"]),
    new Raum("A.E.04", "Übungsraum", "EF42", 40, ["1 Beamer", "Whiteboard", "Tafel"])
  ];

  module.exports = rooms;